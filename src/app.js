import express from 'express'
const app = express()

// --- Route Controllers
import perkRouter from '../routes/perkRouter.js'
import matchRouter from '../routes/matchRouter.js'

// --- Middleware
app.use(express.json())

// for local testing
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*")
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  )
  next()
})

// --- Routes ---
app.use('/api/perks', perkRouter)
app.use('/api/match', matchRouter)

export default app