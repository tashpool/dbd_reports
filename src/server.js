if (process.env.NODE_ENV !== 'production') {
  await import('dotenv/config')
}

import app from './app.js'

// --- Mongo DB Atlas Connection Setup
import mongoose from 'mongoose'
const mongoConnectString = process.env.mongoDB.replace(
  '<PASSWORD>',
  process.env.mongoPassword
)

mongoose.connect(mongoConnectString)
  .catch(error => console.error(error))

let db = mongoose.connection
db.once('open', () => {
  console.log('MongoDB connection established successfully.')
})
db.on('error', console.error.bind(console, 'MongoDB connection error: '))
// --- End Mongo Connection Setup

const port = Number(process.env.PORT) || 8081

// generic routes
app.get('/', (req, res) => {
  res.send('Hello DBD World!')
})

// can we prefetch data from DB here?
// save it locally in a JSON file?

const server = app.listen(port, () => {
  console.log(`DBD Reports starting on port ${port}`)
})