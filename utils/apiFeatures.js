class ApiFeatures {
  constructor(mongooseQuery, expressQuery) {
    // From Express to Mongo { duration: '5', difficulty: easy' }
    this.query = mongooseQuery
    // From Client into Express, ?var1=value1&var2=value2
    this.queryString = expressQuery
  }

  // setup that always run
  filter() {
    const queryObj = { ...this.queryString }
    const excludedFields = ['page', 'sort', 'limit', 'fields']
    excludedFields.forEach(el => delete queryObj[el])

    // advanced filtering
    // make a string type from the object
    let queryStr = JSON.stringify( queryObj )
    queryStr = queryStr.replace(/\b(gte|gt|lte|lt)\b/g, match => `$${match}`)

    // after filtering, return to object
    this.query = this.query.find( JSON.parse(queryStr) )

    return this
  }

  sort() {
    if (this.queryString.sort) {
      // incoming sort: -price,ratingsAverage
      // outgoing sort field: { sort: '-price ratingsAverage' }
      const sortBy = this.queryString.split(',').join(' ')
      this.query = this.query.sort(sortBy)
    } else {
      // default sort field
      this.query = this.query.sort('-startTime')
    }

    return this
  }

  limitFields() {
    if (this.queryString.limit) {
      const fields = this.queryString.split(',').join(' ')
      this.query = this.query.select(fields)
    } else {
      this.query = this.query.select('-__v')
    }

    return this
  }

  // runs by default
  paginate() {
    const page = Number(this.query.page) || 1
    const pageLimit = Number(this.query.limit) || 100
    const entriesToSkip = (page - 1) * pageLimit

    this.query = this.query.skip(entriesToSkip).limit(pageLimit)

    return this
  }

}

// After all blocks, we have the following to be 'await'ed or executed
// query.sort().select().skip().limit()