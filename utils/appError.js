/*
The isOperational value is **only** set with internal errors.
 */
export default class AppError extends Error {
  constructor(message, statusCode) {
    super(message)

    this.statusCode = statusCode
    this.status = `${statusCode}`.startsWith('4') ? 'fail' : 'error'
    this.isOperational = true

    // Add stacktrace and do NOT include this object
    Error.captureStackTrace(this, this.constructor)
  }
}