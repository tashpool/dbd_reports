import mongoose from 'mongoose'
const Schema = mongoose.Schema

const SurvivorSchema = new Schema({
  id: { type: String },
  name: { type: String },
  fullName: { type: String }
})

export default mongoose.model('DRSurvivor', SurvivorSchema)