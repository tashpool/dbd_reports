import mongoose from 'mongoose'
const Schema = mongoose.Schema

const PerkSchema = new Schema({
  name: String
})

// export { PerkSchema as default }
export default mongoose.model('DRPerk', PerkSchema)