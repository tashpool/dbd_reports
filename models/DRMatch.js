import mongoose from 'mongoose'
const Schema = mongoose.Schema

const MatchSchema = new Schema({
  startTime: {
    type: Date,
    default: Date.now(),
    required: true
  },
  // Survivor
  isSurvivor: { type:Boolean, default: true},
  mainSurvivorEscaped: { type: Boolean, default: false},
  //mainSurvivorID: String,
  // [0] is the main player's ID
  teamSurvivorIDs: [ String ],
  numberEscaped: { type: Number, default: 0},
  numberBleedOut: { type: Number, default: 0},
  numberSacrificed: { type: Number, default: 0},
  numberEGCEntity: { type: Number, default: 0},
  numberMori: { type: Number, default: 0 },
  tunneled: { type: Boolean, default: false },
  hookCamped: { type: Boolean, default: false },
  // items
  numberToolboxes: { type: Number, default: 0},
  numberMedkits: { type: Number, default: 0},
  numberMaps: { type: Number, default: 0},
  numberKeys: { type: Number, default: 0},
  numberFlashlights: { type: Number, default: 0},
  // perks
  // mainPerks: [ {type: Schema.Types.ObjectId, ref: 'DRSurvivorPerks'} ],
  // mainPerks: [{
  //   type: String,
  //   enum: perks
  // }],
  mainPerks: [ String ],
  // survivorTwoPerks: [ {type: Schema.Types.ObjectId, ref: 'DRSurvivorPerks'} ],
  // survivorThreePerks: [ {type: Schema.Types.ObjectId, ref: 'DRSurvivorPerks'} ],
  // survivorFourPerks:  [ {type: Schema.Types.ObjectId, ref: 'DRSurvivorPerks'} ],
  // killerPerks: [ {type: Schema.Types.ObjectId, ref: 'DRKillerPerks'} ],
  survivorTwoPerks: [ String ],
  survivorThreePerks: [ String ],
  survivorFourPerks:  [ String ],
  killerID: String,
  killerPerks: [ String ],
  // offerings
  // offerings: [ {type: Schema.Types.ObjectId, ref: 'DROfferings' }],
  bloodpoints: Number,
  offerings: [ String ],
  numberDisconnects: { type: Number, default: 0},
  toxicSurvivors: { type: Boolean, default: false},
  matchNotes: String,
  // location
  realm: String,
  map: String
})

export { MatchSchema as default }