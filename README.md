# Dead By Daylight Reports

## Overview
This app allows you to send DBD (Dead by Daylight) match information into a Nodejs backend, that will be parsed and stored on MongoDB Atlas.

The front end is Vuejs, but the backend is REST based and can handle multiple input formats. 

## Upcoming
The Vuejs frontend will help parse information and also retrieve and display graphical results.

This includes, win and loss percentages, perk saturation, counting addons, character visibility, and more.  

## Deployment
Currently, configured for MongDB Atlas and Heroku hosting. Base directory is Node/Express, and Vuejs front end is built into /client subdirectory.

## Dependencies
See package.json
