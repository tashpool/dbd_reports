import axios from 'axios'

export const axios_instance = axios.create({
  // baseURL: '/api',
  baseURL: 'http://localhost:8081/api',
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

export function postMatch(matchObject) {
  return axios_instance.post('/match', matchObject)
}