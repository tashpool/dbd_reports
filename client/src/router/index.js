import Vue from "vue";
import Router from "vue-router";
import MatchInfo from '@/views/MatchInfo'
import MatchStats from '@/views/MatchStats'

Vue.use(Router)

const routes = [
  {
    path: "/",
    name: "MatchInfo",
    component: MatchInfo,
    meta: { requiresAuth: true }
  },
  {
    path: "/stats",
    name: "MatchStats",
    component: MatchStats,
    meta: { requiresAuth: true }
  },
]

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router



