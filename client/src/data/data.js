export const survivorNames = [
  'dwight fairfield', 'meg thomas', 'claudette morel', 'jake park',
  'nea karlsson', 'laurie strode', 'ace visconti', 'william bill overbeck',
  'feng min', 'david king', 'quentin smith', 'david tapp', 'kate denson',
  'adam francis', 'jefffery jeff johansen', 'jane romero', 'ash j williams',
  'nancy wheeler', 'steve harrington', 'yui kimura', 'zarina kassir',
  'cheryl mason', 'felix richter', 'elodie rakoto', 'yun-jin lee', 'jill valentine',
  'leon scott kennedy', 'mikaela reid', 'jonah vasquez', 'yoichi asakawa',
  'haddie kaur'
]

export const survivorPerks = [
  'ace in the hole', 'adrenaline', 'aftercare', 'alert', 'any means necessary',
  'appraisal', 'autodidact', 'balanced landing', 'bite the bullet', 'blast mine',
  'blood pact', 'boil over', 'bond', 'boon circle of healing', 'boon dark theory',
  'boon exponential', 'boon shadow step', 'borrowed time', 'botany knowledge',
  'breakdown', 'breakout', 'buckle up', 'build to last', 'calm spirit', 'clairvoyance',
  'corrective action', 'counterforce', 'dance with me', 'dark sense', 'dead hard',
  'deception', 'decisive strike', 'deliverance', 'desperate measures',
  'detectives hunch', 'distortion', 'diversion', 'deja vu', 'empathic connection',
  'empathy', 'fast track', 'flashbang', 'flip flop', 'for the people', 'guardian',
  'head on', 'hope', 'inner force', 'inner healing', 'iron will', 'kindred',
  'kinship', 'leader', 'left behind', 'lightweight', 'lithe', 'lucky break',
  'mettle of man', 'no mither', 'no one left behind', 'object of obsession',
  'off the record', 'open handed', 'overcome', 'overzealous', 'parental guidance',
  'pharmacy', 'plunders instinct', 'poised', 'power struggle', 'premonition',
  'prove thyself', 'quick and quiet', 'red herring', 'renewal', 'repressed alliance',
  'residual manifest', 'resilience', 'resurgence', 'rookie spirit', 'saboteur',
  'self aware', 'self care', 'self preservation', 'situational awareness',
  'slippery meat', 'small game', 'smash hit', 'sole survivor', 'solidarity',
  'soul guard', 'spine chill', 'sprint burst', 'stake out', 'streetwise',
  'technician', 'tenacity', 'this is not happening', 'unbreakable', 'up the ante',
  'urban evasion', 'vigil', 'visionary', 'wake up', 'we will make it',
  'we are gonna live forever', 'windows of opportunity'
]

export const killerNames = [
  'trapper', 'wraith', 'hillbilly', 'nurse', 'shape', 'hag', 'doctor', 'huntress',
  'cannibal', 'nightmare', 'pig', 'clown', 'spirit', 'legion', 'plague',
  'ghost face', 'demogorgon', 'oni', 'deathslinger', 'executioner', 'blight',
  'twins', 'trickster', 'nemesis', 'cenobite', 'artist', 'onryo', 'dredge'
]

export const killerPerks = [
  'a nurses calling', 'agitation', 'bamboozle', 'barbecue and chilli', 'beast of prey',
  'bitter murmur', 'blood echo', 'blood warden', 'bloodhound', 'brutal strength',
  'call of brine', 'claustrophobia', 'corrupt intervention', 'coulrophobia',
  'coupe de grace', 'dark devotion', 'darkness revealed', 'dead mans switch', 'deadlock',
  'deathbond', 'deerstalker', 'discordance', 'dissolution', 'distressing', 'dragons grip',
  'dying light', 'enduring', 'eruption', 'fearmonger', 'fire up', 'forced penance',
  'franklins demise', 'furtive chase', 'gearhead', 'grim embrace', 'hangmans trick',
  'hex blood favor', 'hex crowd control', 'hex devour hope', 'hex haunted grounds',
  'hex huntress lullaby', 'hex no one escapes death', 'hex pentimento', 'hex plaything',
  'hex retribution', 'hex ruin', 'hex the third seal', 'hex thrill of the hunt',
  'hex undying', 'hoarder', 'hysteria', 'i am all ears', 'infectious fright', 'insidious',
  'iron grasp', 'iron maiden', 'jolt', 'knock out', 'lethal pursuer', 'lightborn',
  'mad grit', 'make your choice', 'merciless storm', 'monitor and abuse', 'monstrous shrine',
  'nemesis', 'no way out', 'oppression', 'overcharge', 'overwhelming presence',
  'play with your food', 'pop goes the weasel', 'predator', 'rancor', 'remember me',
  'save the best for last', 'scourge hook floods of rage', 'scourge hook gift of pain',
  'scourge hook pain resonance', 'septic touch', 'shadowborn', 'shattered hope',
  'sloppy butcher', 'spies from the shadows', 'spirit fury', 'starstruck', 'stridor',
  'surveillance', 'territorial imperative', 'thanatophobia', 'thrilling tremors', 'tinkerer',
  'trail of torment', 'unnerving presence', 'unrelenting', 'whispers', 'zenshin tactics'
]

export const realmsAndMaps = [
  {
    realm: 'the macmillan estate',
    maps: ['coal tower', 'groaning storehouse', 'ironworks of misery', 'shelter woods', 'suffocation pit']
  },
  {
    realm: 'autohaven wrecker',
    maps: ['azarovs resting place', 'blood lodge', 'gas heaven', 'wreckers yard', 'wretched shop']
  },
  {
    realm: 'coldwind farm',
    maps: ['fractured cowshed', 'rancid abattoir', 'rotten fields', 'the thompson house', 'torment creek']
  },
  {
    realm: 'crotus prenn asylum',
    maps: ['disturbed ward', 'father campbells chapel']
  },
  {
    realm: 'haddonfield',
    maps: ['lampkin lane']
  },
  {
    realm: 'backwater swamp',
    maps: ['the pale rose', 'grim pantry']
  },
  {
    realm: 'lerys memorial institue',
    maps: ['treatment theatre']
  }, {
    realm: 'red forest',
    maps: ['mothers dwelling', 'the temple of purgation']
  },
  {
    realm: 'springwood',
    maps: ['badham preschool i', 'badham preschool ii', 'badham preschool iii', 'badham preschool iv','badham preschool v']
  },
  {
    realm: 'gideon meat plant',
    maps: ['the game']
  },
  {
    realm: 'yamaoka estate',
    maps: ['family residence', 'sanctum of wrath']
  },
  {
    realm: 'ormond',
    maps: ['mount ormond resort']
  },
  {
    realm: 'hawkins national laboratory',
    maps: ['the underground complex']
  },
  {
    realm: 'grave of glenvale',
    maps: ['dead dawg saloon']
  },
  {
    realm: 'silent hill',
    maps: ['midwich elementary school']
  },
  {
    realm: 'raccoon city',
    maps: ['raccoon city police station']
  },
  {
    realm: 'forsake boneyard',
    maps: ['eyrie of crows']
  },
  {
    realm: 'withered isle',
    maps: ['garden of joy']
  }
]