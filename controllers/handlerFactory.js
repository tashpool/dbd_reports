import appError from '../utils/appError.js'

const catchAsync = fn => {
  return (req, res, next) => {
    fn(req, res, next).catch(err => next(err))
  }
}

let getOne = (Model, popOptions) =>
  catchAsync(async(req, res, next) => {
    let query = await Model.findById(req.params.id)
    if (popOptions) query = query.populate(popOptions)
    const doc = await query

    if (!doc) return next(new appError('No document found with that ID.', 404))

    res.status(200).json({
      status: 'success',
      data: {
        data: doc
      }
    })
  })

// TODO: verify authcontroller before delete operation
let deleteOne = (Model) =>
  catchAsync(async(req, res, next) => {
    console.log(`Deleting doc with id: ${req.params.id}`)
    const doc = await Model.findByIdAndDelete(req.params.id)

    if (!doc) return next(new appError('No document found with that ID.', 404))

    res.status(204).json({
      status: 'success',
      data: null
    })
  })

let getAll = Model =>
  catchAsync(async(req, res, next) => {
    // TODO write up a better explanation here, nested GETs
    // let filter = {}
    // if (req.params.caseId) filter = { case: req.params.caseId }

    // const features = new apiFeatures(Model.find(filter), req.query)
    //   .filter()
    //   .sort()
    //   .limitFields()
    //   .paginate()

    // delete me
    console.log(`💥 Get All`)
    console.log(req.query)
    console.log(req.params)

    const doc = await Model.find()
    // const doc = await mongoose.model(Model).find()

    res.status(200).json({
      status: 'success',
      results: doc.length,
      data: {
        data: doc
      }
    })
  })

export { getOne, getAll, deleteOne }