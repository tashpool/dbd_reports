import DRMatchSchema from '../models/DRMatch.js'
import * as handlerFactory from '../controllers/handlerFactory.js'
import mongoose from 'mongoose'
import { body, validationResult } from 'express-validator'
import { survivorNames, killerNames, realmsAndMaps } from '../data/data.js'
import killerID from '../models/DRMatch.js'

const DRMatch = mongoose.model('DRMatch', DRMatchSchema)

let drmatch_list = handlerFactory.getAll(DRMatch)
let drmatch_one = handlerFactory.getOne(DRMatch)
let drmatch_delete = handlerFactory.deleteOne(DRMatch)

// custom validation name check
// express-validation custom not working
let nameCheck = (nameArray, inName) => {
  return nameArray.includes(inName.toString())
}

let drmatch_create = [
  body('matchNotes').isLength({max: 500})
    .trim().withMessage('Match notes max length is 500 characters.'),
  body('teamSurvivorIDs').exists().withMessage('At least one survivor name is required.'),
  body('isSurvivor').exists().withMessage('isSurvivor field required.'),

  async function (req, res, next) {
    // quick debugging
    console.log('\nEntered DRMatch Create')
    console.log(`Req.body: ${JSON.stringify(req.body)}\n`)

    req.body.teamSurvivorIDs.forEach(inName => {
      if (!nameCheck(survivorNames, inName)) {
        const err = new Error('Invalid survivor name ' + inName.toString())
        err.status = 'error'
        err.statusCode = 422

        return next(err)
      }
    })
    if (!nameCheck(killerNames, req.body.killerID)) {
      const err = new Error('Invalid killer name ' + req.body.killerID.toString())
      err.status = 'error'
      err.statusCode = 422

      return next(err)
    }

    let defaultStartTime = req.body.startTime
    if (defaultStartTime === null) {
      defaultStartTime = new Date(Date.now())
    }

    const validationErrorChecks = validationResult(req)

    if (!validationErrorChecks.isEmpty()) {
      console.log(`Match create validation failed`)
      const err = new Error(JSON.stringify(validationErrorChecks))
      err.status = 'error'
      err.statusCode = 422

      return next(err)
    }

    // check for array inputs
    if (req.body.teamSurvivorIDs && !Array.isArray(req.body.teamSurvivorIDs)) {
      const err = new Error('Team IDs must be an array of Object IDs.')
      err.status = 'error'
      err.statusCode = 422

      return next(err)
    }

    let newDRMatch = new DRMatch({
      startTime: defaultStartTime,
      isSurvivor: req.body.isSurvivor,
      mainSurvivorEscaped: req.body.mainSurvivorEscaped,
      // mainSurvivorID: req.body.mainSurvivorID,
      teamSurvivorIDs: req.body.teamSurvivorIDs,
      numberEscaped: req.body.numberEscaped,
      numberBleedOut: req.body.numberBleedOut,
      numberSacrificed: req.body.numberSacrificed,
      numberEGCEntity: req.body.numberEGCEntity,
      numberMori: req.body.numberMori,
      tunneled: req.body.tunneled,
      hookCamped: req.body.hookCamped,
      numberToolboxes: req.body.numberToolboxes,
      numberMedkits: req.body.numberMedkits,
      numberMaps: req.body.numberMaps,
      numberKeys: req.body.numberKeys,
      numberFlashlights: req.body.numberFlashlights,
      mainPerks: req.body.mainPerks,
      survivorTwoPerks: req.body.survivorTwoPerks,
      survivorThreePerks: req.body.survivorThreePerks,
      survivorFourPerks: req.body.survivorFourPerks,
      killerID: req.body.killerID,
      killerPerks: req.body.killerPerks,
      bloodpoints: req.body.bloodpoints,
      offerings: req.body.offerings,
      numberDisconnects: req.body.numberDisconnects,
      toxicSurvivors: req.body.toxicSurvivors,
      matchNotes: req.body.matchNotes,
      realm: req.body.realm,
      map: req.body.map
    })

    try {
      let savedMatch = await newDRMatch.save()
      res.status(200).json({
        status: "success",
        msg: "Match data added to system.",
        data: {
          savedMatch
        }
      })
    } catch(tryError) {
      tryError.status = "error"
      tryError.statusCode = 500
      return next(tryError)
    }
  }
]

export { drmatch_list, drmatch_one, drmatch_create, drmatch_delete }