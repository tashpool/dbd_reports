import DRSurvivor from '../models/DRSurvivor.js'
import * as handlerFactory from '../controllers/handlerFactory.js'

const drsurvivor_list = handlerFactory.getAll(DRSurvivor)
const drsurvivor_detail = handlerFactory.getOne(DRSurvivor)

export { drsurvivor_list, drsurvivor_detail }