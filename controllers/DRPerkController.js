import DRPerk from '../models/DRPerk.js'
import * as handlerFactory from '../controllers/handlerFactory.js'

const drperk_list = handlerFactory.getAll(DRPerk)

export { drperk_list }