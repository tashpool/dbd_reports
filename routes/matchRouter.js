import express from 'express'
import * as matchController from '../controllers/DRMatchController.js'

const router = express.Router()

router.route('/')
  .get(matchController.drmatch_list)
  .post(matchController.drmatch_create)

router.route('/:id')
  .get(matchController.drmatch_one)
  .delete(matchController.drmatch_delete)

export { router as default }