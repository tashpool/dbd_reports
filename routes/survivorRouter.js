import express from 'express'
import * as survivorController from '../controllers/DRSurvivorController.js'

const router = express.Router()

router.route('/')
  .get(survivorController.drsurvivor_list)

router.route('/:id')
  .get(survivorController.drsurvivor_detail)

export { router as default }