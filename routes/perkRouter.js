import express from 'express'
import * as perkController from '../controllers/DRPerkController.js'

const router = express.Router()

router.route('/')
  .get(perkController.drperk_list)

export { router as default }